import React from 'react';

import './style.scss';

const Error404 = () => (
  <main>
    <div className="fourzerofour">
      <div className="fourzerofour__blank__div" />
      <h1 className="fourzerofour__number">404</h1>
      <p className="fourzerofour__text">Not Found</p>
      <div className="fourzerofour__blank__div" />
    </div>
  </main>
);

export default Error404;
